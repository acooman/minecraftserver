# Minecraft server

This repository contains our small minecraft server and the map that goes with it.

## First install

Install git and clone this repository to your computer.

```
git clone git clone git@bitbucket.org:acooman/minecraftserver.git
```

You will need to set up git and ssh to the bitbucket to be able to clone.

Then install a java runtime. Download from here: https://www.oracle.com/java/technologies/javase-jdk16-downloads.html

You need to make sure people can connect to the server. To do this, you need to forward your port

```
Port forwarding is the process of opening up a specific port on your local network, so that incoming traffic can connect to a service. 
In this case, we will be opening up the default Minecraft port, 25565.
In order to port forward, you must have admin access to your local network. Many home routers can be accessed by typing 192.168.1.1 in a web browser. 
Enter the admin credentials, and look for the “port forwarding” settings. This is usually under an “advanced settings”, “advanced setup”, or “networking” tab.
For example, on a NetGear router, port forwarding is found under “Advanced” -> “Advanced Setup” -> “Port Forwarding/Port Triggering”.
Here, you will want to click on “Add Custom Service”. Give the service name anything recognizable, like “Minecraft Server”. 
Under the service type, make sure TCP/UDP is selected. For the internal and external port ranges, enter the default Minecraft port “25565”. 
Finally, for the interal IP address, enter the private IP that you found in step #7. Click apply, and then your changes will be saved.
```

After these three steps, you can launch the server and people should be able to connect.

## Starting the server

Before playing, make sure you have the latest version of the map by pulling the latest version of the git repo:

```
git pull
```

To start the server, you then need to double-click `launch.bat` to start the server.

## After playing

When finished playing, type `save-all` to save the map and close the server.

Then commit and push the latest version of the map to the git repo:

```
git add .
git commit -m "played some more"
git push
```


